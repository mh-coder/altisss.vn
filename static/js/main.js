$(document).ready(function () {
    $('.form-login .auth-sep a').click(function(e) {
        e.preventDefault();
        $('.form-login').hide();
        $('.forgot_pw').show();
    });
    $('.forgot_pw .auth-sep a').click(function(e) {
        e.preventDefault();
        $('.form-login').show();
        $('.forgot_pw').hide();
    });
    $(".reg-pop").click(function (e) {
        $(".mask").fadeIn();
        $('.form-reg-popup').fadeIn();
    }); 
    $('.mask').click(function () {
        $(".mask").fadeOut();
        $('.form-reg-popup').fadeOut();
    });
    $('.close').click(function () {
        $(".mask").fadeOut();
        $('.form-reg-popup').fadeOut();
    });
    $( "#openAcount_birthdate" ).datepicker({ dateFormat: 'dd/mm/yy'});
    $( "#openAcount_licenseRegDate" ).datepicker({ dateFormat: 'dd/mm/yy' });
    $(".fa-calendar.ca_birthdate").click(function(e) {
        $( "#openAcount_birthdate" ).datepicker('show');
    });
    $(".fa-calendar.ca_RegDate").click(function(e) {
        $( "#openAcount_licenseRegDate" ).datepicker('show');
    });
});