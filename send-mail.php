<?php
$fullname = isset($_POST['fullname'])?$_POST['fullname']:'';
$tel = isset($_POST['tel'])?$_POST['tel']:'';
$email = isset($_POST['email'])?$_POST['email']:'';
$content = isset($_POST['msg'])?$_POST['msg']:'';
$lang_lang = isset($_POST['lang_lang'])?$_POST['lang_lang']:'vi';

if($lang_lang == 'en'){
    $msg_rrequired = 'The field is required.';
    $msg_em = 'The email is incorrect';
    $msg_sucess = 'You have successfully contacted';
}else{
    $msg_rrequired = 'Thông tin bắt buộc nhập.';
    $msg_em = 'Bạn nhập email không đúng';
    $msg_sucess = 'Bạn đã liên hệ thành công';
}


$fullname = cleanString($fullname);
$tel = cleanString($tel);
$content = cleanString($content);
//$email = cleanString($email);

if($fullname == '' || $tel == '' || $email == '' || $content == ''){
    $data = array(
        'error'=>0,
        'result' => $msg_rrequired
    );
}elseif (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $data = array(
        'error'=>0,
        'result' => $msg_em
    );
}else{
    $data = array(
        'error'=>1,
        'result' => $msg_sucess
    );
}

if($data['error'] == 1){
    require __DIR__ . '/private/vendor/autoload.php';
    $mail = new \core\swiftmailer\Mail(config('mail', 'movies'));
    $mail->send('support@altisss.vn', "[altisss.vn] Contact", "Contact mail", view('mails/contact.php', [
                'fullname' => $fullname,
                'tel' => $tel,
                'email'=>$email,
                'content'=>$content
            ])
        );
}


function cleanString($string = '', $option = '') {
    if($option == 'output') {
      $strData = trim($string);
      $strData =  stripslashes($strData);
    } else {
      $strData = trim(strip_tags($string));
      if (function_exists('addslashes')) {
        $strData = addslashes($strData);
      }
    }
    $strData = str_replace(array('script', '<?php', '<?','<','>', '?'),'%',$strData);
    $strData = preg_replace('/[^A-Za-z0-9?!]/','%', $strData);
    return $strData;
  }

$myJSON = json_encode($data);

echo $myJSON;