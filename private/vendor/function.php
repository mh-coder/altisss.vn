<?php

if (!function_exists('config')) {

    function config($file, $key)
    {

        $result = require __DIR__ . "/../config/$file.php";

        return $result[$key];
    }

    function view($path, $params = [])
    {
        ob_start();
        extract($params);
        require __DIR__ . "/../app/views/" . $path;
        return ob_get_clean();
    }
}
