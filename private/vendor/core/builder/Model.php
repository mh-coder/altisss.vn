<?php

namespace core\builder;

abstract class Model extends QueryBuilder
{
    protected $table = '';
    protected $attributes = array();
    protected $primary = 'id';


    public function __construct($data = []){
        $this->attributes = $data;

        parent::__construct();
    }

    public function __get($key)
    {
        return $this->attributes[$key];
    }

    public function __set($key, $value)
    {
        $this->attributes[$key] = $value;
    }

    function __isset($key)
    {
        return isset($this->attributes[$key]);
    }

    public function exists(){
        return !empty($this->attributes[$this->primary]);
    }
    public function load(array $data){
        foreach($data as $key=>$value){
            $this->$key = $value;
        }
        return true;
    }
}