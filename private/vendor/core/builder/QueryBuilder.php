<?php

namespace core\builder;

use core\builder\PDOConnection as Connection;
use core\helpers\ArrayHelper;

abstract class QueryBuilder
{
    protected $table = '';
    private $select_query = '*';
    private $where_query;
    private $order_query;
    private $skip_query;
    private $take_query;

    protected $connection = false;

    private $master = [];
    private $clever = [];

    public function __construct()
    {
        $this->setConnection($this->connection);
    }

    private function setConnection($connection = false)
    {
        $this->connection = $connection ? $connection : config('database', 'default');

        $this->master = config('database', 'databases')[$this->connection]['master'];
        $this->clever = config('database', 'databases')[$this->connection]['clever'];
    }

    static function __callStatic($method, $parameters)
    {
        $query = new static;
        return call_user_func_array([$query, $method], $parameters);
    }

    public function __call($method, $parameters)
    {
        return call_user_func_array([$this, "_" . $method], $parameters);
    }

    protected function _select($array = [])
    {
        $select = implode(',', ArrayHelper::addPrefix($this->table . ".", $array));

        $this->select_query = empty($select) ? '*' : $select;
        return $this;
    }

    private function addWhere($column, $operator, $value, $type)
    {
        if (empty($this->where_query)) {
            $this->where_query = ' where ';
        } else {
            $this->where_query .= " $type ";
        }
        if (!is_null($value)) {
            $value = "'" . addcslashes($value, "'") . "'";
        }

        $this->where_query .= " $this->table.$column $operator $value ";

        return $this;
    }

    protected function _where($column, $operator = '=', $value = null)
    {
        return $this->addWhere($column, $operator, $value, 'and');
    }

    protected function _orWhere($column, $operator = '=', $value = null)
    {
        return $this->addWhere($column, $operator, $value, 'or');
    }

    protected function _orderBy($column, $type = 'asc')
    {
        if (empty($this->order_query)) {
            $this->order_query .= " order by ";
        } else {
            $this->order_query .= ',';
        }
        $this->order_query .= " $this->table.$column $type ";
        return $this;
    }

    protected function _skip($skip)
    {
        $this->skip_query = " offset $skip ";
        return $this;
    }

    protected function _take($take)
    {
        $this->take_query = " limit $take ";
        return $this;
    }

    protected function _get()
    {
        $query = "select $this->select_query from $this->table $this->where_query $this->order_query $this->take_query $this->skip_query";

        $arr_data = Connection::connect($this->clever)->execute($query);

        $result = [];

        $class = get_called_class();

        foreach ($arr_data as $data) {
            $result[] = new $class($data);
        }
        return $result;
    }

    protected function _toArray(){
        $query = "select $this->select_query from $this->table $this->where_query $this->order_query $this->take_query $this->skip_query";

        $arr_data = Connection::connect($this->clever)->execute($query);

        return $arr_data;
    }

    protected function _rawSelect($query){
        $arr_data = Connection::connect($this->clever)->execute($query);
        return $arr_data;
    }

    protected function _first()
    {
        $query = "select $this->select_query from $this->table $this->where_query $this->order_query limit 0,1";
        $data = Connection::connect($this->clever)->execute($query, true);

        if(empty($data)){
            return null;
        }
        $class = get_called_class();
        return new $class($data);

    }

    public function save()
    {

        $query = "INSERT INTO $this->table({columns}) VALUES({values}) ON DUPLICATE KEY UPDATE {query}";

        $columns = $values = $temp = '';

        foreach ($this->attributes as $key => $value) {

            $columns .= $this->table . "." . $key . ",";
            $values .= "'" . (addslashes($value)) . "'" . ',';

            if ($key === $this->primary) {
                continue;
            }
            if (is_null($value)) {
                $temp .= $this->table . "." . $key . " = NULL,";
            } else {
                $temp .= $this->table . "." . $key . " = '" . (addslashes($value)) . "',";
            }
        }
        $columns = trim($columns, ',');
        $values = trim($values, ',');
        $query = str_replace('{columns}', $columns, $query);
        $query = str_replace('{values}', $values, $query);

        $temp = trim($temp, ',');
        $query = str_replace('{query}', $temp, $query);

        $id = Connection::connect($this->master)->executeUpdate($query, true);

        !empty($id) && $this->id = $id;
    }
}