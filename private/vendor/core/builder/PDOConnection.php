<?php
namespace core\builder;

class PDOConnection
{
    private $driver = null;
    private $database = null;
    private $host = null;
    private $username = null;
    private $password = null;

    public static function connect($connection)
    {
        $obj = new static;
        
        $obj->driver = $connection['driver'];
        $obj->host = $connection['host'];
        $obj->database = $connection['database'];
        $obj->username = $connection['username'];
        $obj->password = $connection['password'];

        return $obj;
    }

    public function execute($query, $once = false)
    {
        try {
            $stmt = new \PDO("$this->driver:host=$this->host;dbname=$this->database", $this->username, $this->password, array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
            $stmt->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

            $stmt = $stmt->prepare($query);
            $stmt->setFetchMode(\PDO::FETCH_ASSOC);
            $stmt->execute();

            if ($once) {
                $result = $stmt->fetch();
            } else {
                $result = $stmt->fetchAll();
            }
        } catch (\PDOException $e) {
            echo $query;
            $stmt = null;
            die($e->getMessage());
        }

        $stmt = null;
        return $result;
    }

    public function executeUpdate($query)
    {
        try {
            $stmt = new \PDO("$this->driver:host=$this->host;dbname=$this->database", $this->username, $this->password, array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"));
            $stmt->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

            $stmt->exec($query);

            $result = $stmt->lastInsertId();

        } catch (\PDOException $e) {
            echo $query;
            $stmt = null;
            die($e->getMessage());
        }

        $stmt = null;
        return $result;
    }
}


