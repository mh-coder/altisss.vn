<?php
namespace core\helpers;

class ArrayHelper
{
    /**
     * @param string $prefix
     * @param array $array
     * @param bool|false $key (true: add to key, false: add to value)
     */
    public static function addPrefix($prefix = null, $array = [], $key = false)
    {
        if ($key) {
            foreach ($array as $k => $v) {
                $arr[$prefix.$key] = $v;
                unset($array[$key]);
            }
        } else {
            foreach ($array as  &$value) {
                $value = $prefix.$value;
            }
        }
        return $array;
    }
}