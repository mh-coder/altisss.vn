<?php

namespace core\client;

class HttpClient
{
    private $ch = null;
    private $header = [];

    public function init()
    {
        $this->ch = curl_init();
        $this->setOpt(CURLOPT_SSL_VERIFYHOST, false)
            ->setOpt(CURLOPT_SSL_VERIFYPEER, false)
            ->setOpt(CURLOPT_RETURNTRANSFER, true)
            ->setOpt(CURLOPT_COOKIEFILE, __DIR__ . '/cookies.txt')
            ->setOpt(CURLOPT_COOKIEJAR, __DIR__ . '/cookies.txt')
            ->setOpt(CURLOPT_USERAGENT, 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_6_8) AppleWebKit/534.30 (KHTML, like Gecko) Chrome/12.0.742.112 Safari/534.30');

        return $this;
    }

    public function close()
    {
        curl_close($this->ch);
        return $this;
    }

    private function execute()
    {
        if (!empty($this->header)) {
            $this->setOpt(CURLOPT_HTTPHEADER, $this->header);
        }
        $result = curl_exec($this->ch);

        $this->header = [];
        
        return $result;
    }

    public function addHeader($param, $value)
    {
        array_push($this->header, "$param: $value");
        return $this;
    }

    public function setOpt($opt, $value)
    {
        curl_setopt($this->ch, $opt, $value);
        return $this;
    }

    public function post($url, $data, $build = true)
    {
        if ($build == true) {
            $data = http_build_query($data);
        }
        $this->setOpt(CURLOPT_URL, $url)
            ->setOpt(CURLOPT_POST, 1)
            ->setOpt(CURLOPT_POSTFIELDS, $data)
            ->setOpt(CURLOPT_URL, $url);
        $result = $this->execute();

        return $result;
    }

    public function get($url)
    {
        $this->setOpt(CURLOPT_URL, $url);

        $result = $this->execute();
        return $result;
    }
}